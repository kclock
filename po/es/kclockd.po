# Spanish translations for kclockd.po package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the kclock package.
#
# Automatically generated, 2022.
# Eloy Cuadra <ecuadra@eloihr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kclockd\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-07 00:43+0000\n"
"PO-Revision-Date: 2022-09-18 17:58+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ecuadra@eloihr.net"

#: alarm.cpp:73 alarm.h:51
#, kde-format
msgid "Alarm"
msgstr "Alarma"

#: alarm.cpp:77 timer.cpp:67
#, kde-format
msgid "View"
msgstr "Ver"

#: alarm.cpp:80
#, kde-format
msgid "Dismiss"
msgstr "Descartar"

#: alarm.cpp:83
#, kde-format
msgid "Snooze"
msgstr "Posponer"

#: alarmmodel.cpp:222
#, kde-kuit-format
msgctxt "@info"
msgid "Alarm: <shortcut>%1 %2</shortcut>"
msgstr "Alarma: <shortcut>%1 %2</shortcut>"

#: main.cpp:31
#, kde-format
msgid "Don't use PowerDevil for alarms if it is available"
msgstr "No usar PowerDevil para las alarmas si está disponible"

#: main.cpp:46
#, kde-format
msgid "© 2020-2022 KDE Community"
msgstr "© 2020-2022 La comunidad KDE"

#: main.cpp:47
#, kde-format
msgid "Devin Lin"
msgstr "Devin Lin"

#: main.cpp:48
#, kde-format
msgid "Han Young"
msgstr "Han Young"

#: timer.cpp:62
#, kde-format
msgid "Timer complete"
msgstr "Temporizador completado"

#: timer.cpp:63
#, kde-format
msgid "Your timer %1 has finished!"
msgstr "El temporizador %1 ha terminado"

#: xdgportal.cpp:34
#, kde-format
msgid ""
"Allow the clock process to be in the background and launched on startup."
msgstr ""
"Permitir que el proceso del reloj se ejecute en segundo plano y se lance "
"durante el arranque."
